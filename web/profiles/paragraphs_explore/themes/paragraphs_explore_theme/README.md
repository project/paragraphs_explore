CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Misc



INTRODUCTION
------------

This is a Paragraphs Explore distrubtion theme.

The goal of theme is to provide examples of theming paragraphs components and
paragraphs_collection layouts and styles.

Theme is using Bootstrap 5 framework.



REQUIREMENTS
------------

None.



INSTALLATION
------------

 * Install as you would normally install a contributed Drupal theme. See
   [Installing Themes](https://www.drupal.org/docs/8/extending-drupal-8/installing-themes)
   for further information.



CONFIGURATION
-------------

This theme does not use any additional configuration settings for now. Configure
it as any other standard Drupal theme.


BUILDING
--------

```bash
npm install
npm run build
```
