<?php

namespace Drupal\paragraphs_explore_base\Plugin\paragraphs\Conversion;

use Drupal\paragraphs\ParagraphInterface;
use Drupal\paragraphs\ParagraphsConversionBase;

/**
 * Converts a given subtitle paragraph to title paragraphs type.
 *
 * A lossless conversion.
 *
 * @ParagraphsConversion(
 *   id = "paragraphs_explore_base_subtitle_to_title",
 *   label = @Translation("Convert to Title"),
 *   source_type = "subtitle",
 *   target_types = {"title"}
 * )
 */
class ParagraphsSubtitleToTitle extends ParagraphsConversionBase {

  /**
   * {@inheritdoc}
   */
  public function convert(array $settings, ParagraphInterface $original_paragraph, array $converted_paragraph = NULL) {
    return [
      [
        'type' => 'title',
        'paragraphs_title' => [
          'value' => $original_paragraph->get('paragraphs_subtitle')->value,
        ],
      ],
    ];
  }

}
