<?php

namespace DrupalProject\composer;

use Composer\Script\Event;
use Symfony\Component\Filesystem\Filesystem;

/**
 * The composer script handler class.
 */
class ScriptHandler {

  /**
   * Checks if the git pre-commit hook exists, if not create it.
   */
  public static function createGitPreCommitHook(Event $event) {
    $fs = new Filesystem();

    // Do not create a pre-commit hook on the Platform.sh environments.
    if (!getenv('PLATFORM_PROJECT') && !$fs->exists('.git/hooks/pre-commit')) {
      $fs->mkdir('.git/hooks');
      $pre_commit_file = file_get_contents('scripts/pre-commit');
      $fs->dumpFile('.git/hooks/pre-commit', $pre_commit_file);
      $fs->chmod('.git/hooks/pre-commit', 0744);
      $event->getIO()
        ->write("Create a .git/hooks/pre-commit file with chmod 0744");
    }
  }

}
